package PT2019.assignment1.assignment1;

import static org.junit.Assert.*;

import org.junit.Test;


public class testare {
	//public void populare(){
	 Polinom p1=new Polinom();
	 Polinom p2=new Polinom();
	 
	 Monom m1p1=new Monom(3,3);
	 Monom m2p1=new Monom(5,2);
	 Monom m3p1=new Monom(7,1);   //p1 = 3x^3 + 5x^2 + 7x
	 
	 Monom m1p2=new Monom(1,3);
	 Monom m2p2=new Monom(2,2);   //p2= x^3 + 2x^2
	@Test 
	public void testAdunare(){
		 p1.adaugaMonom(m1p1);
		 p1.adaugaMonom(m2p1);
		 p1.adaugaMonom(m3p1);     
		 
		 p2.adaugaMonom(m1p2);
		 p2.adaugaMonom(m2p2);
		Polinom p3= p1.adunaP(p2);
		String s=p3.toString();
		assertEquals("+4X^3+7X^2+7X^1",s);
	}
	
	@Test 
	public void testScadere(){
		 p1.adaugaMonom(m1p1);
		 p1.adaugaMonom(m2p1);
		 p1.adaugaMonom(m3p1);     
		 
		 p2.adaugaMonom(m1p2);
		 p2.adaugaMonom(m2p2);
		Polinom p3= p1.scadeP(p2);
		String s=p3.toString();
		assertEquals("+2X^3+3X^2+7X^1",s);
	}
	
	@Test 
	public void testInmultire(){
		 p1.adaugaMonom(m1p1);
		 p1.adaugaMonom(m2p1);
		 p1.adaugaMonom(m3p1);     
		 
		 p2.adaugaMonom(m1p2);
		 p2.adaugaMonom(m2p2);
		Polinom p3= p1.inmultesteP(p2);
		String s=p3.toString();
		assertEquals("+3X^6+11X^5+17X^4+14X^3",s);
	}
	
	@Test 
	public void testDerivare(){
		 p1.adaugaMonom(m1p1);
		 p1.adaugaMonom(m2p1);
		 p1.adaugaMonom(m3p1);     
		 
		Polinom p3= p1.deriveaza();
		String s=p3.toString();
		assertEquals("+9X^2+10X^1+7X^0",s);
	}
	
	@Test 
	public void testIntegrare(){
		 p1.adaugaMonom(m1p1);
		 p1.adaugaMonom(m2p1);
		 p1.adaugaMonom(m3p1);     
		 
		Polinom p3= p1.integreaza();
		String s=p3.toString();
		assertEquals("+3X^4+5X^3+7X^2",s);
	}
	
}
