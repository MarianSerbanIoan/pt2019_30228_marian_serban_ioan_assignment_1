package PT2019.assignment1.assignment1;
import java.util.ArrayList;
import java.util.Collections;



public class Polinom {

	public ArrayList<Monom> monoame = new ArrayList<Monom>();

	public void adaugaMonom(Monom a) {
		boolean ok = true;
		if (monoame != null) {
			for (Monom b : monoame) {
				if (b.putere == a.putere) {
					int t = monoame.indexOf(b);
					monoame.get(t).coeficient = a.coeficient;
					ok = false;
				}
			}
			if (ok)
				monoame.add(a);
		}
	}
	
	public void addMonom(Monom a){
		monoame.add(a);
	}

	public Polinom adunaP(Polinom p) {

		Polinom rez = new Polinom();
		boolean ok = true;
		for (Monom m : this.monoame) {
			for (Monom n : p.monoame)
				if (m.putere == n.putere) {
					Monom result = m.aduna(n);
					rez.adaugaMonom(result);
					ok = false;
					break;
				}
			if (ok) 
				rez.adaugaMonom(m); // adaug si elementele din primul polinom care nu se gasesc in al doilea
			ok=true;
		}
		
		boolean ok1 = true;
		for (Monom y : p.monoame) {
			for (Monom g : rez.monoame)
				if (y.putere == g.putere) 	// adaug si elementele din al doilea polinom care nu se gasesc in primul
					{ok1 = false; break;}
			if (ok1) 
				rez.adaugaMonom(y);
			ok1 = true;
		}

		Collections.sort(rez.monoame);
		return rez;
	}
	
	public Polinom scadeP(Polinom p) {

		Polinom rez1 = new Polinom();
		boolean ok = true;
		for (Monom m : this.monoame) {
			for (Monom n : p.monoame)
				if (m.putere == n.putere) {
					Monom result1 = m.scade(n);
					rez1.adaugaMonom(result1);
					ok = false;
					break;
				}
			if (ok) 
				rez1.adaugaMonom(m); // adaug si elementele din primul polinom care nu se gasesc in al doilea
			ok=true;
		}
		
		boolean ok1 = true;
		for (Monom y : p.monoame) {
			for (Monom g : rez1.monoame)
				if (y.putere == g.putere) 	// adaug si elementele din al doilea polinom care nu se gasesc in primul
					{ok1 = false; break;}
			if (ok1) {
				Monom r=y.minus();
				rez1.adaugaMonom(r);}
			ok1 = true;
		}

		Collections.sort(rez1.monoame);
		return rez1;
	}
	
	public Polinom inmultesteP(Polinom p){
		Polinom rez2 = new Polinom();
		for(Monom t: this.monoame){
			for(Monom e : p.monoame){
				Monom result2 = t.inmulteste(e);
				rez2.addMonom(result2);
			}
		}
		rez2.simplificaP();
		rez2.simplificaP();
		Collections.sort(rez2.monoame);
		return rez2;
	}
	
//	public Polinom simplificaP(Polinom p){
//		for(int i=0; i<p.monoame.size(); i++)
//		{
//			for(int j=i+1; j<p.monoame.size(); j++){
//				if(p.monoame.get(i).putere == p.monoame.get(j).putere)
//					{
//					p.monoame.get(i).aduna(p.monoame.get(j));
//					p.monoame.remove(j);
//					}	}
//		}
//		Collections.sort(p.monoame);
//		return p;
//	}
	/////////////////
	
	
	public void simplificaP(){
		for(int i=0; i<monoame.size(); i++)
		{
			for(int j=i+1; j<monoame.size(); j++){
				if(monoame.get(i).putere == monoame.get(j).putere)
					{
					monoame.get(i).adunaCoeficientInmultire(monoame.get(j));
					this.monoame.remove(j);
					}	}
		}
		Collections.sort(this.monoame);
	}
	
	public Polinom deriveaza()
	{
		Polinom rez3 = new Polinom();
		for(Monom t: this.monoame){
			Monom result3 = t.deriveazaP();
			rez3.adaugaMonom(result3);
		}
		Collections.sort(rez3.monoame);
		return rez3;
	}
	
	
	public Polinom integreaza()
	{
		Polinom rez4 = new Polinom();
		for(Monom t: this.monoame){
			Monom result4 = t.integreazaP();
			rez4.adaugaMonom(result4);
		}
		Collections.sort(rez4.monoame);
		return rez4;
	}
	
	public void sterge(){
		for(Monom t: this.monoame)
			monoame.remove(t);
	}
	
	public String toString(){
		
		String s = "";
		for(Monom m: this.monoame)
			{if (m.coeficient > 0)
					s = s + "+" + m.coeficient + "X^" + m.putere;
				
			else if(m.coeficient < 0)
				s = s + m.coeficient + "X^" + m.putere;}
	
		return s;
	}
}
