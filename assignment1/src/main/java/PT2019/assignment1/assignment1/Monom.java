package PT2019.assignment1.assignment1;

public class Monom implements Comparable<Monom>{
	public int coeficient;
	public int putere;

	public Monom(int coef, int putere) {
			this.coeficient = coef;
			this.putere = putere;
	}

	public Monom aduna(Monom m){
		
		int x=this.coeficient + m.coeficient;
		Monom rez=new Monom(x,this.putere);
		return rez;
	}
	
	public void adunaCoeficientInmultire(Monom m){
		this.coeficient=this.coeficient +m.coeficient;
	}
	
public Monom scade(Monom m){
		
		int x=this.coeficient - m.coeficient;
		Monom rez1=new Monom(x,this.putere);
		return rez1;
	}
public Monom minus(){
	int x1=this.coeficient*(-1);
	Monom e=new Monom(x1,this.putere);
	return e;
	
}

public Monom inmulteste(Monom m){
	int x=this.coeficient * m.coeficient;
	int y=this.putere + m.putere;
	Monom rez2=new Monom(x,y);
	return rez2;
}

public Monom deriveazaP(){
	int x=this.coeficient * this.putere;
	int y=this.putere -1;
	Monom rez3=new Monom(x,y);
	return rez3;
}

public Monom integreazaP(){
	
	int y=this.putere +1;
	Monom rez3=new Monom(this.coeficient,y);
	return rez3;
}

@Override
public int compareTo(Monom a) {
	// TODO Auto-generated method stub
	int putere = a.putere -this.putere;
	return putere;
}
	
	
	
}
