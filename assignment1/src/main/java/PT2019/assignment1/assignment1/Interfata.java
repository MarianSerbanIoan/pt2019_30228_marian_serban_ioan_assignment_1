package PT2019.assignment1.assignment1;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Interfata {

	public Polinom p1 = new Polinom();
	public Polinom p2 = new Polinom();

	public Interfata() {

		JFrame f = new JFrame("Calculator polinoame");
		f.setSize(600, 600);
		f.setDefaultCloseOperation(f.EXIT_ON_CLOSE);
		JPanel panel1 = new JPanel();
		JLabel jl1 = new JLabel("Polinomul 1 este: ");
		final JTextField ta1 = new JTextField(40);
		JButton sterge1 = new JButton("Sterge P1");
		panel1.add(jl1);
		panel1.add(ta1);
		panel1.add(sterge1);
		panel1.setLayout(new FlowLayout());

		JPanel panel2 = new JPanel();
		JLabel jl2 = new JLabel("Polinom 2 este: ");
		final JTextField ta2 = new JTextField(40);
		JButton sterge2 = new JButton("Sterge P2");
		panel2.add(jl2);
		panel2.add(ta2);
		panel2.add(sterge2);
		panel2.setLayout(new FlowLayout());

		JPanel panel3 = new JPanel();
		JButton btn1 = new JButton("POL 1");
		JLabel jl3 = new JLabel("Coeficientul: ");
		final JTextField tf1 = new JTextField(4);
		JLabel jl4 = new JLabel("Gradul: ");
		final JTextField tf2 = new JTextField(4);
		JButton btn2 = new JButton("POL 2");
		panel3.add(btn1);
		panel3.add(jl3);
		panel3.add(tf1);
		panel3.add(jl4);
		panel3.add(tf2);
		panel3.add(btn2);
		panel3.setLayout(new FlowLayout());

		JPanel panel4 = new JPanel();
		JButton btn3 = new JButton("Adunare");
		final JTextField tf3 = new JTextField(40);
		panel4.add(btn3);
		panel4.add(tf3);
		panel4.setLayout(new FlowLayout());
		
		JPanel panel5 = new JPanel();
		JButton btn4 = new JButton("Scadere");
		final JTextField tf4 = new JTextField(40);
		panel5.add(btn4);
		panel5.add(tf4);
		panel5.setLayout(new FlowLayout());
		
		JPanel panel6 = new JPanel();
		JButton btn5 = new JButton("Inmultire");
		final JTextField tf5 = new JTextField(40);
		panel6.add(btn5);
		panel6.add(tf5);
		panel6.setLayout(new FlowLayout());
		
		JPanel panel7 = new JPanel();
		JButton btn6 = new JButton("Derivare P1");
		final JTextField tf6 = new JTextField(40);
		panel7.add(btn6);
		panel7.add(tf6);
		panel7.setLayout(new FlowLayout());
		
		JPanel panel8 = new JPanel();
		JButton btn7 = new JButton("Integrare P1");
		final JTextField tf7 = new JTextField(40);
		panel8.add(btn7);
		panel8.add(tf7);
		panel8.setLayout(new FlowLayout());

		JPanel panelFinal = new JPanel();
		panelFinal.add(panel1);
		panelFinal.add(panel2);
		panelFinal.add(panel3);
		panelFinal.add(panel4);
		panelFinal.add(panel5);
		panelFinal.add(panel6);
		panelFinal.add(panel7);
		panelFinal.add(panel8);
		panelFinal.setLayout(new BoxLayout(panelFinal, BoxLayout.Y_AXIS));

		btn1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				String coef = new String();
				coef = tf1.getText();
				int a = 0;
				try{
				a = Integer.parseInt(coef);
				}catch(NumberFormatException e){
					JOptionPane.showMessageDialog(null, "Introduceti un numar !!!");
				}
				// if (y>='') //SA PUN CONDITIE SA FIE CIFRE COEF SI PUTERE

				String grad = new String();
				grad = tf2.getText();
				int b= 0;
				try{
				b = Integer.parseInt(grad);
				}catch(NumberFormatException e){
					JOptionPane.showMessageDialog(null, "Introduceti un numar !!!");
				}
				Monom m = new Monom(a, b);
				p1.adaugaMonom(m);
				
				int nr1=0;
				String h = " ";
				for (Monom q : p1.monoame)
					if (q.coeficient > 0)
						if(nr1>0)
						h = h + "+" + q.coeficient + "X^" + q.putere;
						else{
						h = h  + q.coeficient + "X^" + q.putere;
						nr1=nr1+1;}
					else if(q.coeficient < 0){
						h = h + q.coeficient + "X^" + q.putere;
						nr1=nr1+1;}
				ta1.setText(h);
			}
		});
		
		
		btn2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub

				String coef2 = new String();
				coef2 = tf1.getText();
				int a1=0;
				try{
				a1 = Integer.parseInt(coef2);
				}catch(NumberFormatException e){
					JOptionPane.showMessageDialog(null, "Introduceti un numar !!!");
					}
				// if (y>='') //SA PUN CONDITIE SA FIE CIFRE COEF SI PUTERE

				String grad = new String();
				grad = tf2.getText();
				int b1=0;
				try{
				b1 = Integer.parseInt(grad);
				}catch(NumberFormatException e){
					JOptionPane.showMessageDialog(null, "Introduceti un numar !!!");
				}
				Monom m1 = new Monom(a1, b1);
				p2.adaugaMonom(m1);
				
				int nr1=0;
				String h1 = " ";
				for (Monom q : p2.monoame)
					if (q.coeficient > 0)
						if(nr1>0)
							h1 = h1 + "+" + q.coeficient + "X^" + q.putere;
							else{
							h1 = h1  + q.coeficient + "X^" + q.putere;
							nr1=nr1+1;}
					else if(q.coeficient < 0){
						h1 = h1 + q.coeficient + "X^" + q.putere;
						nr1=nr1+1;}
				ta2.setText(h1);
			}
		});
		
		
		btn3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub

				Polinom rezultatAd=new Polinom();
				rezultatAd=p1.adunaP(p2);       
				String h3 = " ";
				int nr1=0;
				for (Monom q : rezultatAd.monoame)
					if (q.coeficient > 0)
						if(nr1>0)
							h3 = h3 + "+" + q.coeficient + "X^" + q.putere;
							else{
							h3 = h3  + q.coeficient + "X^" + q.putere;
							nr1=nr1+1;}
					else if(q.coeficient < 0){
						h3 = h3 + q.coeficient + "X^" + q.putere;
						nr1=nr1+1;}
				tf3.setText(h3);
			}
		});
		
		
		btn4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub

				Polinom rezultatSc=new Polinom();
				rezultatSc=p1.scadeP(p2);       
				String h4 = " ";
				int nr1=0;
				for (Monom q : rezultatSc.monoame)
					if (q.coeficient > 0)
						if(nr1>0)
							h4 = h4 + "+" + q.coeficient + "X^" + q.putere;
							else{
							h4 = h4  + q.coeficient + "X^" + q.putere;
							nr1=nr1+1;}
					else if(q.coeficient < 0){
						h4 = h4 + q.coeficient + "X^" + q.putere;
						nr1=nr1+1;}
				tf4.setText(h4);
			}
		});
		
		
		btn5.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub

				Polinom rezultatInm=new Polinom();
				rezultatInm=p1.inmultesteP(p2);       
				String h5 = " ";
				int nr1=0;
				for (Monom q : rezultatInm.monoame)
					if (q.coeficient > 0)
						if(nr1>0)
							h5 = h5 + "+" + q.coeficient + "X^" + q.putere;
							else{
							h5 = h5  + q.coeficient + "X^" + q.putere;
							nr1=nr1+1;}
					else if(q.coeficient < 0){
						h5 = h5 + q.coeficient + "X^" + q.putere;
						nr1=nr1+1;}
				tf5.setText(h5);
			}
		});
		
		
		btn6.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub

				Polinom rezultatDeriv=new Polinom();
				rezultatDeriv=p1.deriveaza();       
				String h6 = " ";
				int nr1=0;
				for (Monom q : rezultatDeriv.monoame)
					if (q.coeficient > 0)
						if(nr1>0)
							h6 = h6 + "+" + q.coeficient + "X^" + q.putere;
							else{
							h6 = h6  + q.coeficient + "X^" + q.putere;
							nr1=nr1+1;}
					else if(q.coeficient < 0){
						h6 = h6 + q.coeficient + "X^" + q.putere;
						nr1=nr1+1;}
				tf6.setText(h6);
			}
		});
		
		btn7.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub

				Polinom rezultatInteg=new Polinom();
				rezultatInteg=p1.integreaza();       
				String h7 = " ";
				int nr1=0;
				for (Monom q : rezultatInteg.monoame)
					if (q.coeficient > 0)
						if(nr1>0)
								h7 = h7 +  "+(" +q.coeficient +"/" +q.putere + ")" + "X^" + q.putere;
							else{
								h7 = h7 + "("+q.coeficient +"/" +q.putere + ")" + "X^" + q.putere;
							    nr1=nr1+1;}
					else if(q.coeficient < 0){
						q=q.minus();
						h7 = h7 + "-("+q.coeficient +"/" +q.putere + ")" + "X^" + q.putere;
						nr1=nr1+1;}
					
				tf7.setText(h7);
			}
		});

		
		sterge1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub

				p1.sterge();
				String h6 = " ";
				ta1.setText(h6);
			}
		});

		
		sterge2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				p2.sterge();
				String h6 = " ";
				ta2.setText(h6);
			}
		});


		f.setContentPane(panelFinal);
		f.setVisible(true);

	}
}
